#!/bin/sh
#set -ex

rootdir=$(dirname $0)
template=$rootdir/mission_document.html.j2

for f in $rootdir/*.yaml; do
    mdfile=${f%%.yaml}.md
    j2 $template $f -o $mdfile
    echo $mdfile
done
